<?php 
function _close_table($tbl){   	
    $tbl->field_is_closed[LANGUAGE_NONE][0]['value'] = 1;                          
    $playIds = array();
    $rows = array();

    $query = db_select("games","g")->fields("g");
    $query->condition('tid',$tbl->nid);
    $rows = $query->execute()->fetchAll();
          
    $winners = 0;     
    // while(!$winners);  
    $max_errors = (int)$tbl->field_max_errors[LANGUAGE_NONE][0]['value'];
    $errors = 0;
   
   
   $game_type = _get_game_type($tbl);
   
    //calaculate winners when in fast5 mode
   if($game_type =="fast5"){		   
	   $top_games = _get_top_players($tbl);		   
	   foreach($rows as $row){		 
			if(in_array($row->game_id,array_keys($top_games))){
				$row->has_won=true;
			}
	   }
	   $winners = count($top_games);
   }	
   //calaculate winners when in normal mode
   else{
	  while($winners==0&&$max_errors>=$errors){  
      foreach($rows as $row){		 
        if($row->right_answers>=$tbl->field_qnumber[LANGUAGE_NONE][0]['value']-$errors){           
           $row->has_won=true;
           $winners++;
        }
      }
      $errors++;   
	}  
   }	
   $total_won = (float)variable_get('money_won', 0);       
   $field_ballance_type = table_get_ballance_field($tbl ,"win");

   $table_type = table_get_type($tbl);
   $game_win_type = table_get_type($tbl,"win");

   if($winners){
     $pay_back = $tbl->field_win_amount[LANGUAGE_NONE][0]['value'];
     $pay_back_per_user = $pay_back/$winners;  
     //dont give back less then game price
     $min_pay_back = $tbl->field_play_price[LANGUAGE_NONE][0]['value'];
     //unless it is credit-cash game...
	 if($game_win_type!=$table_type){
		 $min_pay_back = 0;
	 }
     if($pay_back_per_user<$min_pay_back){
		 $pay_back_per_user = $min_pay_back;
	 }	 
     $total_won+=$pay_back;  
     $type_sign = $game_win_type=="money"?"$":"C";
     foreach($rows as $row){
        if(isset($row->has_won)&&$row->has_won){   
          $muser = user_load($row->uid);      
          if(!$muser)continue;        
          $muser->{$field_ballance_type}[LANGUAGE_NONE][0]['value']+=$pay_back_per_user;
          watchdog("close_table","user uid:".$row->uid." won. adding ".$pay_back_per_user." to users ".$field_ballance_type." field");
          $row->won =$pay_back_per_user;                     
          user_save($muser);      
          drupal_write_record('games',$row,"game_id");
          /** send congrats email **/
          global $language;
          $msg['win'] = $pay_back_per_user;
          $msg['subject'] = "You have won a Trivia game";           
          $msg_body = variable_get("game_mail_body_".$game_win_type,"");          
          $msg_body = str_replace("%s",round($pay_back_per_user,2).$type_sign,$msg_body);
          $msg_body = str_replace("%gid",$row->game_id,$msg_body);
          $msg['body'] =  $msg_body;
          /*
          $msg['body'] = "Good job!<br/>";
          $msg['body'] .="You have won $".round($pay_back_per_user,2)." in Trivia online game (game id:#".$row->game_id.").<br/>";
          $msg['body'] .="The money has been transferred to your trivia account.";        
          */
          drupal_mail("trivia_game", "win_msg", $muser->mail, $language, $msg);          
        }
	  }
	}
     else {
		 $field_ballance_type = table_get_ballance_field($tbl);
		 foreach($rows as $row){
			$pay_back_per_user = $tbl->field_play_price[LANGUAGE_NONE][0]['value'];
			$muser = user_load($row->uid);     
			if(!$muser)continue; 
			$muser->{$field_ballance_type}[LANGUAGE_NONE][0]['value']+=$pay_back_per_user;
			$row->won =$pay_back_per_user;
			//$total_won+=$pay_back_per_user;       
			user_save($muser);      
			drupal_write_record('games',$row,"game_id");
			/** send congrats email **/
			global $language;
			$msg['win'] = $pay_back_per_user;	
			$msg['subject'] = "You got money from Trivia game";
			$msg_body = variable_get("game_mail_moneyback_".$game_win_type,"");
			$msg_body = str_replace("%s",round($pay_back_per_user,2).$type_sign,$msg_body);
			$msg_body = str_replace("%gid",$row->game_id,$msg_body);
			$msg['body'] =  $msg_body;
			/*  
			$msg['body'] = "Since there were no winner in the game - you have got $".$pay_back_per_user."back in Trivia online game.<br/>The money has been transferred to your trivia account.";       			 
			*/
			drupal_mail("trivia_game", "win_msg", $muser->mail, $language, $msg);  
		 }
	 }
	if($total_won>0){
		variable_set('money_won', $total_won);
	}
        
   $tbl->field_payed[LANGUAGE_NONE][0]['value'] = 1;  
   node_save($tbl);
}
