(function($){
  function mk_countdown(id){
  //  $("#"+id).html($("#"+id).html()+1);  
  }
  $(function(){
    $(".view-games-on tbody tr .views-field-field-closing-time").each(function(i,n){
        var count_down = 0;
        var days = $(this).html().split("d");
        if(days.length>1)
          var count_down = days[0]*86400;
        var time = days[days.length-1].split(":").reverse();
        var multiple = 1;
        $(time).each(function(ii,t){
          count_down+=t*multiple;           
          multiple*=60;
        }); 
                
        $(this).countdown({until:count_down,                           
                           expiryUrl: document.URL,
                           description: ''});         
    });
  });
})(jQuery);
