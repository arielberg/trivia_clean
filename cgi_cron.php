<?php
/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (cron jobs).
 */
 
/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', "/home/triviawin/domains/triviawin.com/public_html");
//if(exec("whoami")!="root"){return;}
$_SERVER['REMOTE_ADDR'] = "127.0.0.1";
$_SERVER['SERVER_SOFTWARE'] = "Apache";
$_SERVER['REQUEST_METHOD'] = "GET";
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
drupal_cron_run();

