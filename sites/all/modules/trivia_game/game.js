var ajax_open = true;
var basic_title = "";
(function($){
 // var originalConsole = console;
  var pass_time = 9;
  $(function(){                       
     val = parseInt($("#total_q").html());
     basic_title = $("h1.title").html();
     if(val<10)val="0"+val;
       $("#total_q").html(val);
     $.get_question();    
  });  
  function disableselect(e){
    return false;
  }
  function reEnable(){
    return true;
  }
  document.onselectstart=new Function('return false');
  if (window.sidebar){
    document.onmousedown=disableselect;
    document.onClick=reEnable;
  }    
  $.get_question = function(){  	
	 if(ajax_open && pass_time >1){
		console.log(ajax_open?"open":"close");
		console.log(pass_time);
		ajax_open = false;
		clearTimeout(timer);	
		console.log("sending" , listCookies());	
		$.ajax({
         type: "POST",
         url: "answer",
         data: "ans="+$.findSelection(),
         success: function(msg){			
			print_answer(msg); 			
			ajax_open = true;			
           },
         error:function(er){			
			var myPath = 'node/38874';			
			window.location = Drupal.settings.basePath + myPath;
		 }
        });
     } 
  }
  var timer;
  $.findSelection = function(){
        ret = 0;        
        
        $(".answer-item").each(function(i,v){					
           if(v.checked)
           {              
              ret = v.value;
           }     
        });        
        return ret;
  }
  
  function print_answer(q)
  {    
     if(q.url) 
     {
      window.location = q.url;
     } 
     else
     {	   
       if(q.i<10)q.i="0"+q.i;
       $("#current_q").html(q.i);
       questions = "<form name='qna'><div id='question'>";
       questions += "<div id='answers'>";
       $.each(q.answers,function(i,val){
           questions += "<div class='answer'>"+
                          "<input class='answer-item' type='radio' name='q' id='q"+(i+1)+"' value='"+(i+1)+"'>"+
                          "<label class='option' for='q"+(i+1)+"'>"+val+"</label>"
                      +"</div>";
       })
       submit_text = "Next Question >";
       if(parseInt(q.i) >= q.i_of){ submit_text = "Finish";}
           
       questions += "</div><input class='q_sumit' type='button' value='"+submit_text+"' "+
					 "ondblclick='return false;' "+                
				   "onClick='jQuery.get_question()'/>";
       questions += "</div></form>";
       $("#question_title").html(q.question);
       var q_img = $("#question_img");
       if(q.img){
			q_img.attr("src","/sites/default/files/"+q.img);
			q_img.css("display","");
	   }
	   else {
		   q_img.css("display","none");
	   }
       $("#answers").html(questions);    
       pass_time = 0;            
       var elm = document.getElementById('timerBar');
		var newone = elm.cloneNode(true);
		elm.parentNode.replaceChild(newone, elm);		 
	    document.getElementById('timerBar').style.animationDuration=q.q_time+'s';
		document.getElementById('timerBar').style.webkitAnimationDuration = q.q_time+'s';
		document.getElementById('timerBar').style['-webkit-transition-duration'] = q.q_time+'s';
	    document.getElementById('timerBar').setAttribute('class','start');
       timer = setTimer(q.q_time);       
     }
  }
  function setTimer(sec)
  {    
	pass_time++;
	sec = parseInt(sec);
    if(sec>=0)
    {
      timer = setTimeout(function(){setTimer(sec-1)},1000);
      if(sec<10)sec="0"+sec;
      $("#time_left").html(sec);
      $("#time_left").attr("class","time"+sec);
      if(sec<2){
		$(".q_sumit").attr("disabled","disabled");
	  }
    } 
    else
    {
      if(sec<=-1)
      {
        $.get_question();
      }
     // timer = setTimeout(function(){setTimer(sec-1)},1000);
    }     
  }
  function listCookies() {
    var theCookies = document.cookie.split(';');
    var aString = '';
    for (var i = 1 ; i <= theCookies.length; i++) {
        aString += i + ' ' + theCookies[i-1] + "\n";
    }
    return aString;
}
})(jQuery);

