(function($){
  $(function(){
     $.get_question();   
  });   
  $.get_question = function(){ 
       if(document.qna){ 
        for (i=0; i<document.qna.q.length; i++){
          if(document.qna.q[i].checked){
           Drupal.settings.questions[qustion_i-1]['answer'] = document.qna.q[i].value - 1;            
          }
        }
       }
       print_answer(Drupal.settings.questions[qustion_i]); 
       qustion_i++;
  }
  var timer;
  var qustion_i = 0;
  function print_answer(q)
  {       
     if(Drupal.settings.questions.length<=qustion_i){
        game_ended();
        return;     
     }
     
      $("h1.title").html("Question "+(qustion_i+1));
      if(qustion_i<9)q_i="0"+(qustion_i+1);
      else q_i=qustion_i+1;
      $("#current_q").html(q_i);
      question = "<form name='qna'><div id='question'>";
      question += "<span id='question_title'>"+q.title+"</span><div id='answers'>";
      $.each(q.options,function(i,val){
      question += "<div class='answer'>"+
                            "<input class='answer-item' type='radio' name='q' id='q"+(i+1)+"' value='"+(i+1)+"'>"+
                            "<label class='option' for='q"+(i+1)+"'>"+val+"</label>"
                        +"</div>";
         })
         question += "</div><input class='q_sumit' type='button' value='Next' onClick='jQuery.get_question()'/>";
         question += "</div></form>";
       $("#question_wrapper").html(question);
       clearTimeout(timer);
       timer = setTimer(10);       
  }
  function game_ended(){
    $("h1.title").html("Game Over");
    var win =1;
    var table_put = "<table id='demo_results'><thead><tr><th>Question</th><th>Your Answer</th><th>Right Answer</th><th>Right</th></tr></thead><tbody>";   
    for(var i = 0 ;i<Drupal.settings.questions.length;i++){
      var is_currect = "V";
      if(Drupal.settings.questions[i].answer != Drupal.settings.questions[i].currect){
        win = 0;
        is_currect = "X";
      }
      if(Drupal.settings.questions[i].options[ans_id]===undefined){
        Drupal.settings.questions[i].options[ans_id] = "";
      }
      var ans_id = Drupal.settings.questions[i].answer;
      var cur_id = Drupal.settings.questions[i].currect;  
      table_put+="<tr><td>"+Drupal.settings.questions[i].title+"</td>"+
                 "<td>"+Drupal.settings.questions[i].options[ans_id]+"</td>"+ 
                 "<td>"+Drupal.settings.questions[i].options[cur_id]+"</td>"+ 
                 "<td class='answer_"+is_currect+"'>"+is_currect+"</td>"+ 
                 "</tr>";     
    } 
    table_put += "<tbody></table>";  
    
    var output = "";  
    if(win){
		 output = '<div class="demo_mesg">Congratulation you have won the game.</div>';
   //   output = '<div class="demo_mesg">Congratulation you have won the game. <br/><span class="play_demo"><a href="'+Drupal.settings.games_url+'">Click here</a></span> to play real game</div><span><a href="'+Drupal.settings.games_url+'"><img class="start_b" src="https://www.trivionaire.com/sites/all/themes/clouds/images/start_botton.png" /></a></span>';
    }
    else {	 
		 output = '<div class="demo_mesg demo_los_msg"><a href="../user/register">That was just a demo game.<br/><b>Join now  to TriviaWin and enjoy the amazing world of trivia!</b></a></div>';
    //  output = '<div class="demo_mesg">Sorry you have lost the game.<br/><span class="play_demo"><a href="'+Drupal.settings.games_url+'">Click here</a></span> to play real game</div><span><a href="'+Drupal.settings.games_url+'"></a></span>';
    }
      buttns = Drupal.settings.buttns;
    $("#block-system-main").html(output+table_put+buttns);  
  }
  function setTimer(sec)
  {    
    if(sec>=0)
    {
      timer = setTimeout(function(){setTimer(sec-1)},1000);
      if(sec<10)sec="0"+sec;
      $("#time_left").html(sec);
      $("#time_left").attr("class","time"+sec);
    } 
    else
    {        
      $.get_question();
    }     
  }
})(jQuery);

