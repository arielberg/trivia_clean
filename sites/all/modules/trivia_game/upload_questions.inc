<?php

function upload_q_csv_page(){
  $form = drupal_get_form("upload_q_csv");
  return $form;
}
function upload_q_csv($form){
	$form['instruction'] = array(
		'#type' => 'markup',
		'#title' => t('Instruction'),	
		'#markup' => "<ul>
						  <li>upload .xlsx file in office 2007 file format or above</li>						  						  
						  <li>Make sure the the first row contains 'Category' ,'Sub-category','Question','A','B','C','D','Level of difficulty','image','link'</li>
						  <li><b>Importent:</b> Questions will appear only after publishing them (in manage tab)!</li>
					  </ul><br/>"
					  ,
					  
		'#weight' => -10,
	);   
	$form['upload'] = array(		
		'#type' => 'file',
		'#title' => t('Upload CSV file'),	
		'#size' => 22,
		'#theme_wrappers' => array(),
		
	);
	$form['submit'] = array('#type' => 'submit', '#value' => t('Upload'));
	return $form;
}

function upload_q_csv_submit($form,$values){
	set_include_path(libraries_get_path("PHPExcel"));
	include 'PHPExcel/IOFactory.php';
	$validators = array();
	$file = $_FILES['files']['tmp_name']['upload'];	
	$inputFileType = 'Excel2007';
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($file);
	$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
	$first_row = array_shift($sheetData);
	$fixedArr = array();
	foreach($sheetData as $data_row){
		$row_by_name = array();
		foreach($first_row as $key=>$k_name){
			$row_by_name[strtolower($k_name)] = $data_row[$key];
		}	
		$fixedArr[] = $row_by_name;	
	}	
    $nodes = array();
    $vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = 'category'")->fetchField();
    foreach($fixedArr as $item){
	    set_time_limit(20);
        if(!$term = taxonomy_get_term_by_name($item['category'])){
          $lbl = _fix_upload_str($item['category']);           
          $term = taxonomy_term_save((object) array(
          'name' => $lbl,
          'vid' => $vid)); 
          $term = taxonomy_get_term_by_name($item['category']);        
          }          
        if($item['sub-category']){
          $lbl = _fix_upload_str($item['sub-category']);  
          if(!$child = taxonomy_get_term_by_name($lbl)){                      
            $res = taxonomy_term_save((object) array(
            'name' => $lbl,
            'vid' => $vid,
            'parent'=>key($term)));         
            $child = taxonomy_get_term_by_name($item['category']);     
               
          }  
        }
        else{			
            $child = $term;     
        }           
        $node = new stdClass();
        $node->title = _fix_upload_str($item['question']);
        $node->type = "question"; 
        node_object_prepare($node);         
        $node->status = 0; 
        $node->language = LANGUAGE_NONE;
        $node->promote = 0; 
        $node->comment = 1;        
        $node->field_category[LANGUAGE_NONE][0]['tid'] = key($child);
        $node->field_difficulty[LANGUAGE_NONE][0]['value'] = $item['level of difficulty'];
        $node->field_answers[LANGUAGE_NONE][0]['value'] = _fix_upload_str($item['a']);
        $node->field_answers[LANGUAGE_NONE][1]['value'] = _fix_upload_str($item['b']);
        $node->field_answers[LANGUAGE_NONE][2]['value'] = _fix_upload_str($item['c']);
        $node->field_answers[LANGUAGE_NONE][3]['value'] = _fix_upload_str($item['d']); 
		if($item['image']){
			$node->field_q_img[LANGUAGE_NONE][0]['value'] = $item['image'];
		}
		if($item['link']){
			$node->	field_link[LANGUAGE_NONE][0]['value'] = $item['link'];
		}
		$nodes_arr[] = $node;           
  }
  $nid_arr = array();
  foreach($nodes_arr as $node){
	node_save($node);
	$nid_arr[] = $node->nid;
  }
  $old_nid_arr = variable_get("csv_uploads",array());
  $old_nid_arr[] = $nid_arr;
  variable_set("csv_uploads",$old_nid_arr);
  drupal_set_message(t("%num items has been uploaded",array("%num"=>count($nid_arr))));
  return "done"; 	
}
function manage_csv_uploads(){
	  $cur_url = current_path();
	  $csv_arr = variable_get("csv_uploads",array());
	  if(isset($_GET['op'])){
		 $nodes =  $csv_arr[$_GET['rid']];
		 switch($_GET['op']){
			case "published":
				foreach($nodes as $nid){
					$node = node_load($nid);
					$node->status = 1;
					node_save($node);
				}
			    pa("questions Published");
			break;
			case "unpublished":
				foreach($nodes as $nid){
					$node = node_load($nid);
					$node->status = 0;
					node_save($node);
				}
			    pa("questions Unpublished");
			break;
			case "delete":				
			    pa("Currently Unsupported");
			break;
		 }
		 drupal_goto( $cur_url);
	  }
	  $output = "<table><thead><tr><td>id</td><td>nodes</td><td>actions</td></tr></thead><tbody>";
	  foreach($csv_arr as $rid=>$banch){
		  $output.="<tr>";
		  $output.="<td>#".$rid."</td>";
		  $output.="<td>";
		  if(isset($_GET['view']) && $_GET['view'] == $rid){
			foreach($banch as $nid){
				$output.=l($nid,"node/".$nid)." ";
			}
		  }
		  else {
			   $output.=l("...",$cur_url,array("query"=>array("view"=>$rid)));
		  }
		  $output.="</td>";
		  $output.="<td>";		  
			  $output.=l("Unpublished",$cur_url,array("query"=>array("rid"=>$rid,"op"=>"unpublished")));
			  $output.= " | ";
			  $output.=l("Published",$cur_url,array("query"=>array("rid"=>$rid,"op"=>"published")));
			  $output.= " | ";
			  $output.=l("Delete",$cur_url,array("query"=>array("rid"=>$rid,"op"=>"delete")));
		  $output.="</td>";
		  $output.="</tr>";
		  
	  }
	  $output .= "</tbody></table>";
	  return $output;
}
function _fix_upload_str($in){ 
    $out =  trim($in);    
    return $out;
}
