<?php
function manage_csv_uploads(){
		$form = drupal_get_form("triv_q_upload_images");
		return $form;
}


function triv_q_upload_images($form){	
	$form['path'] = array(
        '#title' => 'Upload directory',
        '#type' => 'textfield',
        '#default_value' => '/uploaded',  
        '#description' => 'All images will be uploaded to this directory (in public://)',
    );	
	$form['image'] = array(
        '#title' => 'Upload image',
        '#type' => 'mfw_managed_file',
        '#upload_location' => 'public://',
        '#upload_validators' => array(
            'file_validate_is_image' => array(),
            'file_validate_extensions' => array('png gif jpg jpeg'),
            'file_validate_size' => array(2 * 1024 * 1024),
        ),        
    );       
	$form['submit'] = array(
		'#type'=>'submit',
		'#default_value'=> t('submit')
	);
	drupal_add_css("
	#edit-image-upload-button { display:none; }
	","inline");
	return $form;
}
function triv_q_upload_images_validate($form){
	//pa($form_values);
//	pa($_FILES);
}
function triv_q_upload_images_submit($form , $form_values){	
	$field_name = 'image';
	$directory = "public:/".$form_values['values']['path'];
	file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
	$count = 0;
	foreach($_FILES['files']['name'][$field_name] as $k=>$name){
		$image = file_get_contents($_FILES['files']['tmp_name'][$field_name][$k]);
		$file = file_save_data($image, $directory."/".$_FILES['files']['name'][$field_name][$k], FILE_EXISTS_RENAME);	
		$count++;
	}	
	drupal_set_messege("!count files has been uploaded to !dir",array('!count'=>$count,'dir'=>$directory));	
}
